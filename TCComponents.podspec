#
# Be sure to run `pod lib lint Components.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TCComponents'
  s.version          = '0.5.8'
  s.summary          = '组件库'
  s.swift_version    = '5.0'
  #  s.swift_versions = ['4.0', '4.2', '5.0']
# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
欧阳平常开发组件库，各个业务分成不同的组件，组件之间的依赖用cocoapods进行管理，组件之间尽量解耦，便于后续项目根据需要进行集成。
                       DESC

  s.homepage         = 'https://gitee.com/ouxiaojia/Components'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'ouyanghongjia' => 'yanghongjia@163.com' }
  s.source           = { :git => 'https://gitee.com/ouxiaojia/Components.git', :tag => s.version.to_s}
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  #s.source_files = 'Components/Classes/**/*'
  
  # s.resource_bundles = {
  #   'Components' => ['Components/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.subspec 'Mediator' do |a|
      a.source_files = 'Components/Classes/Mediator/**/*'
  end
  
  s.subspec 'Category' do |a|
      a.source_files = 'Components/Classes/Category/UIKit/**/*','Components/Classes/Category/Foundation/**/*'
  end
  
  s.subspec 'UIKit' do |a|
      a.source_files = 'Components/Classes/UIKit/**/*'
      a.dependency 'TCComponents/Category'
  end
  
end
