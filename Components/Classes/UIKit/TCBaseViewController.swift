//
//  TCBaseViewController.swift
//  CocoaPodsDemo
//
//  Created by TianChenIMac1 on 2019/9/6.
//  Copyright © 2019 hamga.com. All rights reserved.
//

import UIKit

//导航栏距离顶部距离
fileprivate func navigationBarTop() -> CGFloat {
    var height = UIScreen.main.bounds.height
    switch UIApplication.shared.statusBarOrientation {
    case .portrait, .portraitUpsideDown, .unknown:
        break
    default:
        height = UIScreen.main.bounds.width
    }
    
    if height >= 812 {
        return 44
    } else {
        return 20
    }
}

open class TCBaseViewController: UIViewController {
    private var tcNavigationBar : TCNavigationBar?
    public var navigationBar : UINavigationBar? { return tcNavigationBar }
    lazy public private(set) var tcNavigationItem : UINavigationItem = UINavigationItem()
    
    /// 返回按钮样式
    open var backItemStyle : ItemStyle = .Text(NSAttributedString.attributed("返回"))
    open var isShowBarLine : Bool {
        get { return tcNavigationBar?.isShowBarLine ?? true }
        set { tcNavigationBar?.isShowBarLine = newValue }
    }
    
    /// 导航栏透明度
    open var transparency : CGFloat{
        set { tcNavigationBar?.transparency = newValue }
        get { return tcNavigationBar?.transparency ?? 1.0 }
    }
    
    deinit {
        removeObserver(self, forKeyPath: "title", context: nil)
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        addObserver(self, forKeyPath: "title", options: [.new, .initial], context: nil)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        
        addObserver(self, forKeyPath: "title", options: [.new, .initial], context: nil)
    }
    
    //MARK: - 视图生命周期
    override open func viewDidLoad() {
        super.viewDidLoad()

        //隐藏系统导航栏
        navigationController?.navigationBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.delegate = navigationController as? UIGestureRecognizerDelegate
        
        //判断是否在导航栈内
        if let _ = navigationController {
            tcNavigationBar = TCNavigationBar()
            view.addSubview(tcNavigationBar!)
            
            tcNavigationBar?.frame = .init(x: 0, y: navigationBarTop(), width: view.frame.width, height: 44)
            tcNavigationBar?.setItems([tcNavigationItem], animated: true)
        }
        
        //是否需要显示返回按钮
        if let vcs = navigationController?.viewControllers, vcs.count > 1 {
            setLeftItem(style: self.backItemStyle, target: self, action: #selector(backItemAction(_:)))
        }
        
        setupSubViews()
    }
    
    //MARK: - 公有方法
    /// 返回按钮触发方法
    /// - Parameter sender: 返回按钮
    @objc open func backItemAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - 设置导航栏按钮便利方法
    @discardableResult
    open func setLeftItem(style: ItemStyle, target: Any?, action: Selector?) -> UIBarButtonItem {
        let item : UIBarButtonItem = .item(with: style, target: target, action: action)
        tcNavigationItem.leftBarButtonItem = item
        return item
    }
    
    @discardableResult
    open func setRightItem(style: ItemStyle, target: Any?, action: Selector?) -> UIBarButtonItem {
        let item : UIBarButtonItem = .item(with: style, target: target, action: action)
        tcNavigationItem.rightBarButtonItem = item
        return item
    }
    
    @discardableResult
    open func addLeftItem(style: ItemStyle, target: Any?, action: Selector?) -> UIBarButtonItem {
        let item : UIBarButtonItem = .item(with: style, target: target, action: action)
        guard let _ = tcNavigationItem.leftBarButtonItems else {
            tcNavigationItem.leftBarButtonItem = item
            return item
        }

        tcNavigationItem.leftBarButtonItems?.append(item)
        return item
    }
    
    @discardableResult
    open func addRightItem(style: ItemStyle, target: Any?, action: Selector?) -> UIBarButtonItem {
        let item : UIBarButtonItem = .item(with: style, target: target, action: action)
        guard let _ = tcNavigationItem.rightBarButtonItems else {
            tcNavigationItem.rightBarButtonItem = item
            return item
        }
        
        tcNavigationItem.rightBarButtonItems?.append(item)
        return item
    }
    
    //MARK: - 监听视图控制器的title变化，实时改变导航栏的title
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let path = keyPath, path == "title" {
            tcNavigationItem.title = change?[NSKeyValueChangeKey.newKey] as? String
            return
        }
        
        super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
    }
    
    open override func willAnimateRotation(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        switch toInterfaceOrientation {
        case .portrait, .portraitUpsideDown, .unknown:
            UIView.animate(withDuration: duration) {
                self.navigationBar?.frame = .init(x: 0, y: navigationBarTop(), width: self.view.frame.width, height: 44)
            }
        default:
            UIView.animate(withDuration: duration) {
                self.navigationBar?.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: 44)
            }
        }
    }
    
    open func setupSubViews() {
        
    }
}
