//
//  TCNavigationBar.swift
//  CocoaPodsDemo
//
//  Created by TianChenIMac1 on 2019/9/6.
//  Copyright © 2019 hamga.com. All rights reserved.
//

import UIKit

public class TCNavigationBar: UINavigationBar {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupSubViews()
    }
    
    private func setupSubViews() {
        barTintColor = .white
        isTranslucent = false
    }
    
    public override var barPosition: UIBarPosition
        {
        return .topAttached
    }
    
    override public func layoutSubviews() {
        super .layoutSubviews()
        
        findLine(self)?.isHidden = !isShowBarLine
        let _ = findBackgroundView(self)?.compactMap({ (view) -> Void in
            view.alpha = transparency
        })
    }
    
    /// 导航栏透明度
    public var transparency : CGFloat = 1.0 {
        didSet {
            let _ = findBackgroundView(self)?.compactMap({ (view) -> Void in
                view.alpha = transparency
            })
        }
    }
    
    /// 是否显示底部分割线
    public var isShowBarLine : Bool = true {
        didSet {
            findLine(self)?.isHidden = !isShowBarLine
        }
    }
    
    //找出导航栏底部分割线
    private func findLine(_ view: UIView) -> UIView? {
        for sub in view.subviews {
            if let img = sub as? UIImageView, img.frame.height < 1.0 {
                return img
            }
            
            if let img = findLine(sub) {
                return img
            }
        }
        return nil
    }
    

    //找出导航栏背景视图
    private func findBackgroundView(_ view: UIView) -> [UIView]? {
        for sub in view.subviews {
            if let cls = object_getClass(sub) {
                if NSStringFromClass(cls) == "_UIBarBackground" {
                    var targets = [sub]
                    targets.append(contentsOf: sub.subviews)
                    return targets
                }
            }
            
            if let targets = findBackgroundView(sub) {
                return targets
            }
        }
        return nil
    }
}
