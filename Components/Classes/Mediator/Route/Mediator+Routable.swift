//
//  Mediator+Routable.swift
//  FBSnapshotTestCase
//
//  Created by TianChenIMac1 on 2019/9/4.
//

import UIKit

fileprivate let routeTargetsKey : UnsafeRawPointer! = UnsafeRawPointer.init(bitPattern: "routeTargetsKey".hashValue)
fileprivate let replacePatternsKey : UnsafeRawPointer! = UnsafeRawPointer.init(bitPattern: "replacePatternsKey".hashValue)

//Mediator路由存储协议实现
extension TCMediator: MediatorRouterStoreType {
    public func addRoute(_ target: MediatorTargetRoutable.Type) {
        var targets = self.routeTargets
        targets.append(target)
        objc_setAssociatedObject(self, routeTargetsKey, targets, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    public func replace(_ url: URLConvertible, pattern: URLConvertible) {
        var patterns = self.replacePatterns
        patterns[url.pattern] = pattern
        objc_setAssociatedObject(self, replacePatternsKey, patterns, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    public var routeTargets: [MediatorTargetRoutable.Type] {
        get {
            if let targets = objc_getAssociatedObject(self, routeTargetsKey) as? [MediatorTargetRoutable.Type] {
                return targets
            }
            objc_setAssociatedObject(self, routeTargetsKey, [], .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            return []
        }
    }
    
    public var replacePatterns: [String : URLConvertible] {
        get {
            if let patterns = objc_getAssociatedObject(self, replacePatternsKey) as? [String : Any] {
               let filters = patterns.compactMapValues { (value) -> URLConvertible? in
                    if let str = value as? String {
                        return str
                    } else if let url = value as? URL {
                        return url
                    }
                    return nil
                }
                return filters
            }
            
            objc_setAssociatedObject(self, replacePatternsKey, [:], .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            return [:]
        }
    }
    
}

// Mediator的路由方法实现
extension TCMediator: MediatorRoutable {
    public func register(_ targetType: MediatorTargetRoutable.Type) {
        addRoute(targetType)
    }
    
    public func replace(url: URLConvertible, with replacer: URLConvertible) {
        replace(url, pattern: replacer)
    }
    
    public func targetType(of url: URLConvertible) -> MediatorTargetType? {
        let url = self.replacePatterns[url.pattern] ?? url
        guard let routable = routeTargets.compactMap({ $0.init(url: url) }).first else { return nil  }
        return routable
    }
    
    public func viewController(of url: URLConvertible) -> UIViewController? {
        let url = self.replacePatterns[url.pattern] ?? url
        guard let target = self.targetType(of: url) else { return nil }
        return self.viewControlller(of: target)
    }
}


// Mediator的路由跳转操作
extension TCMediator {
    @discardableResult
    public func push(_ url: URLConvertible, from: UINavigationController? = nil, animated: Bool = true) -> UIViewController? {
        guard let target = self.targetType(of: url) else { return nil }
        return self.push(target, from: from, animated: animated)
    }
    
    @discardableResult
    public func present(_ url: URLConvertible, from: UIViewController? = nil, wrap: UINavigationController.Type? = nil, animated: Bool = true, completion: (() -> Void)? = nil) -> UIViewController? {
        guard let target = self.targetType(of: url) else { return nil }
        return self.present(target, from: from, wrap: wrap, animated: animated, completion: completion)
    }
    
}

