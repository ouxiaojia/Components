//
//  MediatorRoutable.swift
//  FBSnapshotTestCase
//
//  Created by TianChenIMac1 on 2019/9/4.
//

import Foundation

/// Mediator管理的业务模块路由协议
public protocol MediatorTargetRoutable : MediatorTargetType {
    init?(url: URLConvertible)
}

//Mediator路由存储协议
public protocol MediatorRouterStoreType {
    func addRoute(_ target : MediatorTargetRoutable.Type)
    func replace(_ url : URLConvertible, pattern : URLConvertible)
    var routeTargets: [MediatorTargetRoutable.Type] { get }
    var replacePatterns: [String: URLConvertible] { get }
}

/// Mediator的路由协议
public protocol MediatorRoutable where Self: MediatorRouterStoreType {
    /// 业务模块注册到Mediator
    /// - Parameter targetType: 业务
    /// - Returns: 无返回值
    func register(_ targetType: MediatorTargetRoutable.Type)
    
    /// 路由切换
    /// - Parameters:
    ///   - url: 原始路由
    ///   - replacer: 替换后的路由
    /// - Returns: <#return value description#>
    func replace(url: URLConvertible, with replacer: URLConvertible)
    
    func targetType(of url: URLConvertible) -> MediatorTargetType?
    func viewController(of url: URLConvertible) -> UIViewController?
}
