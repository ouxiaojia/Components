//
//  TCMediator.swift
//  SecurityCloud
//
//  Created by TianChenIMac1 on 2019/9/2.
//  Copyright © 2019 TianChenIMac1. All rights reserved.
//

import UIKit

public let Mediator = TCMediator()

open class TCMediator {
    @discardableResult
    public func push(_ target: MediatorTargetType, from: UINavigationController? = nil, animated: Bool = true) -> UIViewController? {
        guard let viewController = self.viewControlller(of: target) else { return nil }
        guard let navigationController = from ?? UIViewController.topViewController?.navigationController else { return nil }
        navigationController.pushViewController(viewController, animated: animated)
        return viewController
    }
    
    @discardableResult
    public func present(_ target: MediatorTargetType, from: UIViewController? = nil, wrap: UINavigationController.Type? = nil, animated: Bool = true, completion: (()->Void)? = nil) -> UIViewController? {
        //presented ViewController
        guard let viewController = self.viewControlller(of: target) else { return nil }
        //presenting ViewController
        guard let fromViewController = from ?? UIViewController.topViewController else { return nil }
        
        //是否需要UINavigationController包装一层
        let viewControllerToPresent : UIViewController
        if let navigationControllerClass = wrap, (viewController is UINavigationController) == false {
            viewControllerToPresent = navigationControllerClass.init(rootViewController: viewController)
        } else {
            viewControllerToPresent = viewController
        }
        
        fromViewController.present(viewControllerToPresent, animated: animated, completion: completion)
        return viewController
    }
    
}

extension TCMediator : MediatorType {
    public func viewControlller(of target: MediatorTargetType) -> UIViewController? {
        guard let viewController = target.viewController else { return nil }
        return viewController
    }
}
