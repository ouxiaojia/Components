//
//  MediatorTargetType.swift
//  SecurityCloud
//
//  Created by TianChenIMac1 on 2019/9/2.
//  Copyright © 2019 TianChenIMac1. All rights reserved.
//

import UIKit

// 业务模块要遵循的协议
public protocol MediatorTargetType {
    var viewController : UIViewController? { get }
}

//Mediator 遵循协议
public protocol MediatorType {
    func viewControlller(of target: MediatorTargetType) -> UIViewController?
}
