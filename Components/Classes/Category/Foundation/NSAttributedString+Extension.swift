//
//  NSAttributedString+Extension.swift
//  CocoaPodsDemo
//
//  Created by TianChenIMac1 on 2019/9/6.
//  Copyright © 2019 hamga.com. All rights reserved.
//

import UIKit

extension NSAttributedString {
    public class func attributed(_ text: String, color: UIColor = .blue, fontSize: CGFloat = 17) -> NSAttributedString {
        let attributedText = NSMutableAttributedString.init(string: text)
        return attributedText.foreground(color: color).font(size: fontSize)
    }
}

extension NSMutableAttributedString {
    public func foreground(color: UIColor = .blue) -> NSMutableAttributedString {
        addAttribute(.foregroundColor, value: color, range: .init(location: 0, length: self.string.count))
        return self
    }
    
    public func font(size: CGFloat = 17) -> NSMutableAttributedString {
        addAttribute(.font, value: UIFont.systemFont(ofSize: size), range: .init(location: 0, length: self.string.count))
        return self
    }
    
    public func foreground(color: UIColor = .blue, range: NSRange) -> NSMutableAttributedString {
        addAttribute(.foregroundColor, value: color, range: range)
        return self
    }
    
    public func font(size: CGFloat = 17, range: NSRange) -> NSMutableAttributedString {
        addAttribute(.font, value: UIFont.systemFont(ofSize: size), range: range)
        return self
    }
}
