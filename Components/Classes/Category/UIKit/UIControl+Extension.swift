//
//  UIControl+Extension.swift
//  DelegateTest
//
//  Created by TianChenIMac1 on 2019/8/29.
//  Copyright © 2019 Baidu.com. All rights reserved.
//

import UIKit

private protocol TargetAction {
    func performAction(_ control: UIControl)
}

private class TargetActionWrapper : TargetAction {
    deinit { action = nil }
    var action: ((UIControl) -> ())?
    init(action:@escaping (UIControl) -> ()) {
        self.action = action
    }
    
    func performAction(_ control: UIControl) {
        if let at = action {
            at(control)
        }
    }
}

var actionsKey : String?
extension UIControl {
    /// 添加事件响应
    ///
    /// - Parameters:
    ///   - event: 事件类型
    ///   - action: 响应回调函数
    public func add(event: UIControl.Event = .touchUpInside, action:@escaping ((UIControl) -> ())) {
        if var actionEventDic = objc_getAssociatedObject(self, &actionsKey) as? Dictionary<String, Any> {
            actionEventDic["\(event.rawValue)"] = TargetActionWrapper(action: action)
        } else {
            let actionEventDic = ["\(event.rawValue)":TargetActionWrapper(action: action)]
            objc_setAssociatedObject(self, &actionsKey, actionEventDic, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        self.addTarget(self, action: selector(for: event), for: event)
    }
    
    
    private func performAction(for controlEvent: UIControl.Event) {
        if let actionEventDic = objc_getAssociatedObject(self, &actionsKey) as? Dictionary<String, Any> {
            if let targetAction = actionEventDic["\(controlEvent.rawValue)"] as? TargetActionWrapper {
                targetAction.performAction(self)
            }
        }
    }
    
    @objc private func selector(for event: UIControl.Event) -> Selector {
        switch event {
        case .touchDown:
            return #selector(onControlTouchDown(_:))
        case .touchDownRepeat:
            return #selector(onControlTouchDownRepeat(_:))
        case .touchDragInside:
            return #selector(onControlTouchDragInside(_:))
        case .touchDragOutside:
            return #selector(onControlTouchDragOutside(_:))
        case .touchDragEnter:
            return #selector(onControlTouchDragEnter(_:))
        case .touchDragExit:
            return #selector(onControlTouchDragExit(_:))
        case .touchUpInside:
            return #selector(onControlTouchUpInside(_:))
        case .touchUpOutside:
            return #selector(onControlTouchUpOutside(_:))
        case .touchCancel:
            return #selector(onControlTouchCancel(_:))
        case .editingDidBegin:
            return #selector(onControlEditingDidBegin(_:))
        case .editingChanged:
            return #selector(onControlEditingChanged(_:))
        case .editingDidEnd:
            return #selector(onControlEditingDidEnd(_:))
        case .editingDidEndOnExit:
            return #selector(onControlEditingDidEndOnExit(_:))
        default:
            return #selector(onControlOtherEvent(_:))
        }
    }
    
    @objc private func onControlTouchDown(_ control: UIControl) {
        performAction(for: .touchDown)
    }
    
    @objc private func onControlTouchDownRepeat(_ control: UIControl) {
        performAction(for: .touchDownRepeat)
    }
    
    @objc private func onControlTouchDragInside(_ control: UIControl) {
        performAction(for: .touchDragInside)
    }
    
    @objc private func onControlTouchDragOutside(_ control: UIControl) {
        performAction(for: .touchDragOutside)
    }
    
    @objc private func onControlTouchDragEnter(_ control: UIControl) {
        performAction(for: .touchDragEnter)
    }
    
    @objc private func onControlTouchDragExit(_ control: UIControl) {
        performAction(for: .touchDragExit)
    }
    
    @objc private func onControlTouchUpInside(_ control: UIControl) {
        performAction(for: .touchUpInside)
    }
    
    @objc private func onControlTouchUpOutside(_ control: UIControl) {
        performAction(for: .touchUpOutside)
    }
    
    @objc private func onControlTouchCancel(_ control: UIControl) {
        performAction(for: .touchCancel)
    }
    
    @objc private func onControlValueChanged(_ control: UIControl) {
        performAction(for: .valueChanged)
    }
    
    @objc private func onControlEditingDidBegin(_ control: UIControl) {
        performAction(for: .editingDidBegin)
    }
    
    @objc private func onControlEditingChanged(_ control: UIControl) {
        performAction(for: .editingChanged)
    }
    
    @objc private func onControlEditingDidEnd(_ control: UIControl) {
        performAction(for: .editingDidEnd)
    }
    
    @objc private func onControlEditingDidEndOnExit(_ control: UIControl) {
        performAction(for: .editingDidEndOnExit)
    }
    
    @objc private func onControlOtherEvent(_ control: UIControl) {
        performAction(for: .allEvents)
    }
}


extension UIButton {
    /// 点击事件添加
    ///
    /// - Parameters:
    ///   - event: 事件类型
    ///   - action: 响应函数，带有一个参数
    public func touch(_ event: UIControl.Event = .touchUpInside, action: @escaping ((UIControl) -> ())) {
        super.add(event: event, action: action)
    }
    
    
    /// 点击事件添加
    ///
    /// - Parameters:
    ///   - event: 事件类型
    ///   - action: 响应函数，无参数
    public func touch(_ event: UIControl.Event = .touchUpInside, action: @escaping (() -> ())) {
        let newAction : (UIControl) -> () = { _ in
            action()
        }
        super.add(event: event, action: newAction)
    }
}
