//
//  UIBarButtonItem+Extension.swift
//  CocoaPodsDemo
//
//  Created by TianChenIMac1 on 2019/9/6.
//  Copyright © 2019 hamga.com. All rights reserved.
//

import UIKit

public enum ItemStyle {
    case Picture(_ image: String, _ contentHorizontalAlignment: UIControl.ContentHorizontalAlignment = .center)
    case Text(_ text: NSAttributedString)
    case System(_ item: UIBarButtonItem.SystemItem, _ width: CGFloat)
    case CustomView(_ view: UIView)
}

extension UIBarButtonItem {
    public class func item(with style: ItemStyle, target: Any?, action: Selector?) -> UIBarButtonItem {
        switch style {
        case let .System(s, w):
            let item = UIBarButtonItem.init(barButtonSystemItem: s, target: target, action: action)
            item.width = w
            return item
        case .Picture(let image, let alignment):
            let item = UIButton.init(type: .custom)
            if let ac = action {
                item.addTarget(target, action: ac, for: .touchUpInside)
            }
            item.setImage(UIImage.init(named: image), for: .normal)
            item.setImage(UIImage.init(named: image), for: .selected)
            item.setImage(UIImage.init(named: image), for: .highlighted)
            item.contentHorizontalAlignment = alignment
            return .init(customView: item)
        case .Text(let text):
            let item = UIButton.init(type: .roundedRect)
            if let ac = action {
                item.addTarget(target, action: ac, for: .touchUpInside)
            }
            item.setAttributedTitle(text, for: .normal)
            return .init(customView: item)
        case .CustomView(let view):
            let item = UIBarButtonItem.init(customView: view)
            return item
        }
    }
}
