//
//  MainService.swift
//  Components_Example
//
//  Created by TianChenIMac1 on 2019/9/5.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import Foundation
import TCComponents

enum MainService : MediatorTargetType {
    case normalPush
    case normalPresent
    case routePush
    case routePresent
    var viewController: UIViewController?
    {
        switch self {
        case .normalPush:
            return PushViewController()
        case .normalPresent:
            return PresentViewController()
        case .routePush:
            return PushRouteViewController()
        case .routePresent:
            return PresentRouteViewController()
        }
    }
}

extension MainService : MediatorTargetRoutable {
    init?(url: URLConvertible) {
        switch url.pattern {
        case "sy://home/normalPush":
            self = .normalPush
        case "sy://home/normalPresent":
            self = .normalPresent
        case "sy://home/routePush":
            self = .routePush
        case "sy://home/routePresent":
            self = .routePresent
        default:
            return nil
        }
    }
}
