//
//  ViewController.swift
//  Components
//
//  Created by skybluewq on 09/04/2019.
//  Copyright (c) 2019 skybluewq. All rights reserved.
//

import UIKit

class ViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let home = BaseNavigationController(rootViewController: HomeViewController())
        home.tabBarItem.title = "首页"
        let mine = BaseNavigationController(rootViewController: MineViewController())
        mine.tabBarItem.title = "我的"
        
        addChildViewController(home)
        addChildViewController(mine)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

