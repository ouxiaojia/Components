//
//  HomeViewController.swift
//  Components_Example
//
//  Created by TianChenIMac1 on 2019/9/5.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import TCComponents

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        Mediator.register(MainService.self)
        Mediator.replace(url: "sy://home/routePush", with: "sy://home/routePresent")
        // Do any additional setup after loading the view.
        let normalPushBtn = UIButton(type: .roundedRect)
        normalPushBtn.setTitle("normal Push", for: .normal)
        normalPushBtn.touch {
            Mediator.push(MainService.normalPush)
        }
        
        let normalPresentBtn = UIButton(type: .roundedRect)
        normalPresentBtn.setTitle("normal Push", for: .normal)
        normalPresentBtn.touch {
            Mediator.push(MainService.normalPresent)
        }
        
        let routePushBtn = UIButton(type: .roundedRect)
        routePushBtn.setTitle("Route Push", for: .normal)
        routePushBtn.touch {
            Mediator.push(MainService.routePush)
        }
        
        let routePresentBtn = UIButton(type: .roundedRect)
        routePresentBtn.setTitle("Route Present", for: .normal)
        routePresentBtn.touch {
            Mediator.push(MainService.routePresent)
        }
        
        let stackView = UIStackView(arrangedSubviews: [normalPushBtn, normalPresentBtn, routePushBtn, routePresentBtn])
        stackView.axis = .vertical
        stackView.spacing = 50
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        view.addSubview(stackView)
        
        stackView.center = view.center
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
